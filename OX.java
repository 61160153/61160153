import java.util.Scanner;


public class OX {
	public static int row, col;
    public static Scanner scan = new Scanner(System.in);
    public static char[][] board = {{'_','_','_'},{'_','_','_'},{'_','_','_'}};
    public static char turn = 'X';
    public static int count = 0;

    public static void showWelcome(){
        System.out.println("Welcome to OX games...");
    } 

    public static void showTable(){
        for (int i = 0; i < 3; i++) {
            System.out.println();
            for (int j = 0; j < 3; j++) {
                if (j == 0)
                    System.out.print("| ");
                System.out.print(board[i][j] + " | "); 
            }
        }
        System.out.println();
    }
    
    public static void showTurn(){
        System.out.println("Turn : " + turn);		  
    }
    
    public static void switchTurn(){
        if (turn == 'X') 
            turn = 'O';
        else
            turn = 'X';
    }
    
    public static void showInput(){
        System.out.println("Please enter a row and colum : ");
        row = scan.nextInt();
        col = scan.nextInt(); 
        row--;
        col--;
        count++;
        checkInput();
    }
    public static void checkInput() {
    	if(checkError() == true){
            showInput();
        }else{
            setTable();
        }
    }
    public static void setTable() {
    	board[row][col] = turn;
    }
    
    public static boolean checkError(){
        if(row < 0  || row > 2){
            showError();
            return true;
        }else if(col < 0 || col > 2){
            showError();
            return true;
        }else if((col > 2 || col < 0) && (row > 2 || row < 0)) {
        	showError();
            return true;
        }
        else
            return false;
        }
    public static void showError() {
        System.out.println("error, please input 1 or 2 or 3");
    }
        
    public static boolean playGame() {
        for(int i = 0 ;i < 3 ;i++){
            if(checkRow()){
                Win();
                return true;
            }else if(checkCol()){
                Win();
                return true;
            }else if(checkDiagonal()){
                Win();
                return true;
            }
        } return false;
    }

    public static boolean checkRow() {
        for(int i = 0 ;i < 3 ;i++){
            if(board[0][i]==turn && board[1][i]==turn && board[2][i]==turn)
                return true;
        }
        return false;		
    }

    public static boolean checkCol() {
        for(int i = 0 ;i < 3 ;i++){
            if(board[i][0]==turn && board[i][1]==turn && board[i][2]==turn)
                return true;
        }
        return false;		
    }
    
    public static boolean checkDiagonal() {
        if(checkDiagonal1()) {
            return true;
        }if(checkDiagonal2()) {
            return true;
        }
        return false;
    }

    public static boolean checkDiagonal1() {
        for(int i = 0 ;i < 3 ;i++){
            if(board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[1][1] != '_')
                return true;
        }
        return false;
    }
    
    public static boolean checkDiagonal2() {
        for(int i = 0 ;i < 3 ;i++){
            if(board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[1][1] != '_')
                return true;
        }
        return false;
    }
    
    public static boolean checkDraw(){
        if(count == 9){
            showTable();
            showDraw();
            showThank();
            showBye();
            return true;
        }
        return false;
    }
    public static void Win(){
        showTable();
        showWin();
        showThank();
        showBye();
    }
    public static void showThank(){
        System.out.println("Thank you....");
    }
    private static void showBye() {
		System.out.println("Bye Bye.....");	
    }
    public static void showWin() {
    	System.out.println("Game over! Player " + turn + " wins!");
    }
    public static void showDraw() {
    	System.out.println("It's a tie.");
    }

    public static void main(String[] args) {
       showWelcome();
       for(;;){
           showTable();
           showTurn();
           showInput();
           if(playGame()==true){
               break;
           }else if(checkDraw()==true){
               break;
           }
           switchTurn();
       }
    }
}
